# frozen_string_literal: true

module Idioms
  class Engine < ::Rails::Engine
    isolate_namespace Idioms
    config.generators.api_only = true
  end
end
