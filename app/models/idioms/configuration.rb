# frozen_string_literal: true

module Idioms
  class Configuration < WidgetInstanceConfiguration
    attribute :show_title, :boolean, default: true
    attribute :show_author, :boolean, default: true
    attribute :show_message, :boolean, default: true
    attribute :show_date, :boolean, default: true

    validates :show_title, :show_author, :show_message, :show_date, boolean: true
  end
end
