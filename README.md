# mirr.OS one Idioms Widget

## Installation
Add this line to your mirrOS_api's Gemfile:

```ruby
gem 'idioms'
```

And then execute:
```bash
$ bundle
```

Or install it via the mirrOS_settings web interface.

## More information
Visit [glancr.de](https://glancr.de) for more information.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
