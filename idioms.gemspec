# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'idioms/version'

Gem::Specification.new do |s|
  s.name        = 'idioms'
  s.version     = Idioms::VERSION
  s.authors     = ['Marco Roth']
  s.email       = ['marco.roth@intergga.ch']
  s.homepage    = 'https://glancr.de/widgets/idioms'
  s.summary     = 'Idioms mirr.OS one Widget'
  s.description = 'Shows quotes or idioms on your mirror'
  s.license     = 'MIT'
  s.metadata    = {
    'json' => {
      type: 'widgets',
      title: {
        enGb: 'Idioms',
        deDe: 'Sprüche'
      },
      description: {
        enGb: s.description,
        deDe: 'Zeigt in zeitlich festgelegten Abständen Zitate oder Sprüche auf deinem Spiegel an.'
      },
      sizes: [
        {
          w: 6,
          h: 4
        }
      ],
      languages: %i[enGb deDe],
      group: 'idiom_collection'
    }.to_json
  }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails', '~> 5.2'
end
